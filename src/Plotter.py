import json
import math
import matplotlib
import numpy as np
import pandas as pd
import plotly.graph_objects as go
from matplotlib import pyplot as plt
from matplotlib.dates import DateFormatter
from matplotlib.ticker import PercentFormatter
from pandas import DatetimeIndex

from src.densitygraph import densityGraph
from src.util import *

# TODO: I can figure out what each of these do and document it, if someone wants me to. Ask me if you need it pls


def scatter_pickle(pickle_file, x_name, y_name, title, color_name=None, output_file="scatterplot.jpg"):
    df = load_entire_df_pickle(pickle_file)
    fig = plt.figure()
    x_values = df[x_name]
    y_values = df[y_name]
    ax = fig.add_axes([0, 0, 1, 1])
    if color_name is None:
        ax.scatter(x=x_values, y=y_values, c=range(len(x_values)), alpha=0.05, s=1)
    else:
        something = ax.scatter(x=x_values, y=y_values, c=df[color_name], alpha=0.2, s=1)
        cbar = plt.colorbar(something)

        cbar.ax.get_yaxis().set_ticks([])
        for j, lab in enumerate(['$0$', '$1$', '$2$', '$>3$']):
            cbar.ax.text(.5, (2 * j + 1) / 8.0, lab, ha='center', va='center')
        cbar.ax.get_yaxis().labelpad = 15
        cbar.ax.set_ylabel('# of contacts', rotation=270)

    ax.set_ylabel("Total Production (in W)")
    ax.set_xlabel("Clearsky Irradiation (in W)")
    ax.set_title(title)
    X = [100, 100]
    Y = [0, 300000]
    ax.plot(X, Y, linestyle="dashed", linewidth=1.5, color='red')

    fig.savefig(plot_folder + output_file, bbox_inches='tight', dpi=150)
    plt.show()


def hour_day_thing(pickle_file, date_column, color_column):
    with open(pickle_file, 'rb') as infile:
        df = pickle.load(infile)

    fig = plt.figure()
    dates = df[date_column]
    dti = DatetimeIndex(dates)
    y_values = dti.hour * 60 + dti.minute
    x_values = dti.dayofyear
    color = df[color_column]
    ax = fig.add_axes([0, 0, 1, 1])
    ax.scatter(x=x_values, y=y_values, c=color, alpha=0.1, marker='.')
    ax.set_xlabel('Day of year')
    ax.set_ylabel('Minute of day')
    ax.set_title('PV Energy Production by time')

    fig.savefig(plot_folder + 'scatterplot.jpg', bbox_inches='tight', dpi=150)
    plt.show()


def lab_thing(pickle_file, date_column, production_column):
    with open(pickle_file, 'rb') as infile:
        df = pickle.load(infile)

    fig = plt.figure()
    dates = df[date_column]
    dti = DatetimeIndex(dates)
    y_values = df[production_column]
    x_values = dti
    ax = fig.add_axes([0, 0, 1, 1])
    ax.scatter(x=x_values, y=y_values, s=1, alpha=1, marker='.')
    ax.set_xlabel('Day of year')
    ax.set_ylabel('DC Production')
    ax.set_title('PV Energy Production by time')

    fig.savefig(plot_folder + 'lab_production.jpg', bbox_inches='tight', dpi=150)
    plt.show()


def contour_thing(pickle_file, x_name, y_name, title, color_name=None, output_file="scatterplot.jpg"):
    with open(pickle_file, 'rb') as infile:
        df = pickle.load(infile)

    xlist = df[x_name]
    ylist = df[y_name]
    X, Y = np.meshgrid(xlist, ylist)
    Z = df[color_name]
    fig, ax = plt.subplots(1, 1)
    cp = ax.contourf(X, Y, Z)
    fig.colorbar(cp)
    ax.set_title(title)
    ax.set_xlabel(x_name)
    ax.set_ylabel(y_name)
    fig.savefig(plot_folder + output_file, bbox_inches='tight', dpi=150)
    plt.show()


def html_plot(predicted, expected, dates):
    import plotly.io as pio
    pio.renderers.default = "browser"

    fig = go.Figure()
    fig.add_trace(go.Scatter(x=dates, y=predicted, mode='lines', name='predicted'))
    fig.add_trace(go.Scatter(x=dates, y=expected, mode='lines', name='expected'))
    fig.update_layout(barmode='stack')
    fig.show()
    fig.write_html(plot_folder + "lineplot.html")

    df = pd.DataFrame()
    df["date"] = dates
    df["exp"] = expected
    df["prd"] = predicted


def ratio_scatter(predicted, expected, dates, figsize=(10, 10)):
    plt.figure(figsize=figsize)
    ss = 1
    a = 0.2
    expected_divisor = [1 if i < 1 else i for i in expected]
    plt.scatter(expected, np.array(predicted) / np.array(expected_divisor), c=range(len(expected)), s=ss, alpha=a)
    plt.xlabel("expected")
    plt.ylabel("predicted")
    plt.ylim([0, 2])
    plt.grid()
    plt.savefig(plot_folder + "ratio_scatter.png")
    plt.show()


def histogram(x, x_label, bins=100, plotname="histogram", figsize=(10, 10)):
    plt.figure(figsize=figsize)
    plt.xlim(x.min(), x.max())
    plt.hist(x=x, bins=bins, alpha=0.75)
    plt.xlabel(x_label)
    plt.ylabel("Occurences")
    plt.savefig(plot_folder + plotname + ".png")
    plt.show()


def histogram_2d(x, y, x_label, y_label, bins=100, plotname="2d_histogram"):
    plt.figure()
    plt.xlim(x.min(), x.max())
    plt.hist2d(x=x, y=y, bins=bins)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.savefig(plot_folder + plotname + ".png")
    plt.show()


def simple_scatter(predicted, expected, dates, sigma=None, five_percent_lines=True, plot_name="scatter", bin_amount=50,
                   figsize=(10, 10)):
    """
    Creates a scatter plot showing the predicted and expected values
    """
    highest = 0
    for item in predicted:
        if item > highest:
            highest = item

    if sigma:
        bins = [[] for item in range(0, bin_amount)]
        binsize = int(highest / bin_amount) + 1
        lowborders = []
        highborders = []
        ycoords = []

        for i in range(0, len(predicted)):
            bins[int(predicted[i] / binsize)].append(expected[i])

        for i in range(0, len(bins)):
            if bins[i]:
                lowborders.append(np.percentile(bins[i], sigma / 2))
                highborders.append(np.percentile(bins[i], 100 - sigma / 2))
                ycoords.append(i * binsize + binsize / 2)

    st = qickAndDirtyStationarity(dates, expected, badlimit=0.25)

    a = 0.2
    plt.figure(figsize=figsize)

    plt.scatter(expected, predicted, c=st, s=st, alpha=a)
    if sigma:
        plt.plot(lowborders, ycoords, '--', c='b')
        plt.plot(highborders, ycoords, '--', c='r')
    if five_percent_lines:
        plt.plot([0, 500], [0, 500], '-', lw=1, c='#999999')
        plt.plot([0, 475], [0, 500], '-', lw=1, c='#BBBBBB')
        plt.plot([0, 525], [0, 500], '-', lw=1, c='#BBBBBB')
    plt.xlabel("expected")
    plt.ylabel("predicted")
    plt.grid()
    # square plot
    plt.axis('square')
    plt.savefig(plot_folder + plot_name + "%dbins.png" % bin_amount)
    plt.show()


def scatter_two_features(feature1, feature1_name, feature2, feature2_name, plotname):
    plt.figure()
    a = 0.2
    plt.subplots()
    plt.scatter(feature1, feature2, alpha=a)
    plt.colorbar()
    plt.xlabel(feature1_name)
    plt.ylabel(feature2_name)
    plt.title("Scatterplot of %s vs %s" % (feature1_name, feature2_name))
    plt.grid()
    plt.savefig(plot_folder + plotname + ".png")
    plt.show()


def simple_scatter_but_vertical(predicted, expected, dates, sigma=None, fivePercentLines=True, plotName="scatter",
                                color_data=None):
    highest = 0
    for item in predicted:
        if item > highest:
            highest = item

    if sigma:
        binsize = predicted.size / 4
        bins = [[] for item in range(0, int(highest / binsize) + 1)]
        lowborders = []
        highborders = []
        ycoords = []

        for i in range(0, len(predicted)):
            bins[int(predicted[i] / binsize)].append(expected[i] - predicted[i])

        for i in range(0, len(bins)):
            if bins[i]:
                lowborders.append(np.percentile(bins[i], sigma / 2))
                highborders.append(np.percentile(bins[i], 100 - sigma / 2))
                ycoords.append(i * binsize)

    if (color_data is None):
        colors = qickAndDirtyStationarity(dates, expected, badlimit=0.25)
    else:
        colors = color_data

    ss = 1
    a = 0.2
    f, ax = plt.subplots()
    plt.scatter(expected - predicted, predicted, c=colors, s=ss, alpha=a)

    if sigma:
        plt.plot(lowborders, ycoords, '--', c='b')
        plt.plot(highborders, ycoords, '--', c='r')
        plt.title("Scatterplot with %i%s of data within lines" % (100 - sigma, '%'))
    else:
        plt.title("Scatterplot of predicted vs expected values")
    plt.colorbar(label="Stability")
    plt.xlabel("expected")
    plt.ylabel("predicted")
    plt.grid()
    plt.savefig(plot_folder + plotName + ".png")
    plt.show()


def abline(slope, intercept):
    """Plot a line from slope and intercept"""
    axes = plt.gca()
    x_vals = np.array(axes.get_xlim())
    y_vals = intercept + slope * x_vals
    plt.plot(x_vals, y_vals, '--')


def boxplot(predicted, expected, notch=False, name="boxplot.png"):
    # Creating dataset
    data = predicted - expected

    flierprops = dict(marker='.', markerfacecolor='green', markersize=2, linestyle='none')
    plt.figure(figsize=(15, 15))
    plt.title("Boxplot of Predicted - Expected Production")

    plt.boxplot(data, notch=notch, flierprops=flierprops)
    #plt.boxplot(data, notch=notch, flierprops=flierprops)
    plt.savefig(plot_folder + name)
    plt.show()

def create_all_plots(predicted, expected, dates, title):
    print("Creating plots...")
    figsize = (8, 8)
    simple_scatter(predicted, expected, dates, sigma=20, plot_name="scatter80", bin_amount=70, figsize=figsize)
    simple_scatter(predicted, expected, dates, sigma=10, plot_name="scatter90", bin_amount=70, figsize=figsize)
    simple_scatter(predicted, expected, dates, sigma=5, plot_name="scatter95", bin_amount=70, figsize=figsize)
    simple_scatter(predicted, expected, dates, sigma=1, plot_name="scatter99", bin_amount=70, figsize=figsize)
    histogram(predicted - expected, "Deviation of prediction from expected value (absolute)",
              plotname="histogram_absolute_error", figsize=figsize)
    ratio_scatter(predicted, expected, dates, figsize=figsize)

    print("Plotting finished!")

def create_all_plots_alt(predicted, expected, dates, title):
    print("Creating plots...")
    #simple_scatter(predicted, expected, dates, sigma=5, plotName="scatter95")
    #simple_scatter_but_vertical(predicted, expected, dates, sigma=5, plotName="scatter95")
    #simple_scatter(predicted, expected, dates, sigma=20, plotName="scatter80", bin_amount=20)
    #simple_scatter(predicted, expected, dates, sigma=20, plotName="scatter80", bin_amount=50)
    simple_scatter(predicted, expected, dates, sigma=20, plot_name="scatter80", bin_amount=70)
    simple_scatter(predicted, expected, dates, sigma=10, plot_name="scatter90", bin_amount=70)
    simple_scatter(predicted, expected, dates, sigma=5, plot_name="scatter95", bin_amount=70)
    simple_scatter(predicted, expected, dates, sigma=1, plot_name="scatter99", bin_amount=70)

    #simple_scatter(predicted, expected, dates, sigma=20, plotName="scatter80", bin_amount=200)
    #simple_scatter(predicted, expected, dates, sigma=40, plotName="scatter60")
    #boxplot(predicted, expected)
    """ratio_scatter(predicted, expected, dates)
    html_plot(predicted, expected, dates)
    boxplot(predicted, expected)
    
    densityGraph(np.array(predicted), np.array(predicted) / np.array(expected), [0, 50000], [0, 2], baseline=1, blur=0,
                 bins=['dyn', 'dyn'], color="grey",
                 filename='density.png', xlabel="Current", ylabel="P AC/DC", weights=[])

    expected_divisor = [1 if i < 1 else i for i in expected]
    histogram_2d(predicted, expected, "Predicted", "Expected", plotname="histogram_predicted_expected")
    histogram(predicted - expected, "Deviation of prediction from expected value (absolute)",
              plotname="histogram_absolute_error")
    histogram(predicted / expected_divisor, "Deviation of prediction from expected value (in %)",
              plotname="histogram_percent_error")"""



def add_identity(axes, shift=0, *line_args, **line_kwargs):
    identity, = axes.plot([], [], *line_args, **line_kwargs)

    def callback(axes):
        low_x, high_x = axes.get_xlim()
        low_y, high_y = axes.get_ylim()
        low = max(low_x, low_y)
        high = min(high_x, high_y) * (1.2)
        identity.set_data([low, high], [low, high])

    callback(axes)
    axes.callbacks.connect('xlim_changed', callback)
    axes.callbacks.connect('ylim_changed', callback)
    return axes


def qickAndDirtyStationarity(times, P, badlimit=0.25):
    n = min(len(times), len(P))
    c = np.ones(n)
    for i in range(n - 1):
        if i == 0:
            continue
        d2 = 0
        if P[i - 1] is not None and P[i + 1] is not None and P[i] is not None:
            d2 = P[i - 1] + P[i + 1] - 2 * P[i]

        if P[i] == 0:
            continue
        badchange = badlimit * P[i]
        badness = np.clip(math.fabs(d2) / badchange, 0, 1)
        c[i] = badness

    d = c

    # additionally let any badness spread sidewards
    for gg in range(2):
        for i in range(n - 1):
            if i == 0:
                continue
            d[i] = max((d[i] + d[i - 1]) * 0.5, d[i])
            d[i] = max((d[i] + d[i + 1]) * 0.5, d[i])

    return 1 - d


def time_circle_days(pickle_file):
    df = load_entire_df_pickle(pickle_file)
    fig = plt.figure()
    x_values = df['sin_days']
    y_values = df['cos_days']

    plt.figure(figsize=(15, 15))
    a = 1
    plt.subplots()
    plt.scatter(x_values, y_values, alpha=a, c=[item.day_of_year for item in df['_Date']])
    plt.colorbar(label="Day in year")
    feature1_name = 'day_sin'
    feature2_name = 'day_cos'
    plt.xlabel(feature1_name)
    plt.ylabel(feature2_name)
    plt.title("Day in year transformed with circle functions")
    plt.grid()
    plt.savefig(plot_folder + "timecycle_days.png")
    plt.show()


def time_circle_secs(pickle_file):
    df = load_entire_df_pickle(pickle_file)
    fig = plt.figure()
    x_values = df['sin_seconds']
    y_values = df['cos_seconds']

    #plt.figure(figsize=(15, 15))
    a = 1
    plt.subplots()
    plt.scatter(x_values, y_values, alpha=a, c=[item.hour for item in df['_Date']])
    plt.colorbar(label="Hour in day")
    feature1_name = 'sec_sin'
    feature2_name = 'sec_cos'
    plt.xlabel(feature1_name)
    plt.ylabel(feature2_name)
    plt.title("Seconds of day transformed with circle functions")
    plt.grid()
    plt.savefig(plot_folder + "timecycle_secs.png")
    plt.show()


def histogram_test(pickle_file, column_name, column_desc, bins=30, plotname="testohisto", percent_mode=True):
    df = load_entire_df_pickle(pickle_file)
    x = df[column_name]

    fig, ax = plt.subplots()
    #ax.set_xlim(xmin=0)

    #plt.figure(figsize=(15, 15))
    #plt.xlim(x.min(), x.max())

    if percent_mode:
        plt.hist(x=x, weights=np.ones(len(x)) / len(x), bins=bins, edgecolor='black')
        plt.gca().yaxis.set_major_formatter(PercentFormatter(1))
    else:
        plt.hist(x=x, bins=bins, alpha=0.75)

    plt.xlabel(column_desc)
    plt.savefig(plot_folder + plotname + ".png")
    plt.show()


def bar_and_line(pickle_file, bar_column_name, bar_column_desc, line_column_name, line_column_desc, date_column="_Date", plot_name="testobarline"):
    """
    Creates a plot with bars and a line to compare two features to each other
    :param pickle_file: The file to read the data from
    :param bar_column_name: The column to create bars for
    :param bar_column_desc: Description of the bar column to print on the plot
    :param line_column_name: The column to create the line plot for
    :param line_column_desc: Description of the line column to print on the plot
    :param date_column: Column containing the dates in pickle_files
    :param plot_name: file name of the plot (without file ending)
    """
    df = load_entire_df_pickle(pickle_file)

    plt.rcParams["figure.figsize"] = [7.50, 3.50]
    plt.rcParams["figure.autolayout"] = True

    grouped_df = df.groupby(pd.PeriodIndex(df[date_column], freq="M")).mean()
    grouped_df.index = grouped_df.index.to_series().astype(str)

    bar_x = grouped_df[bar_column_name]
    line_x = grouped_df[line_column_name]

    fig, ax = plt.subplots()

    lns1 = ax.bar([thing for thing in range(len(bar_x))], bar_x, label=bar_column_desc)
    plt.xticks(rotation=45)
    ax2 = ax.twinx()
    ax.set_ylabel(bar_column_desc)
    lns2 = ax2.plot(bar_x.index, line_x, color="black", marker="*", label=line_column_desc)
    ax2.set_ylabel(line_column_desc)

    ax.set_ylim(ymin=0)
    ax2.set_ylim(ymin=0)

    lns = [lns1] + lns2
    labs = [l.get_label() for l in lns]
    ax.legend(lns, labs, loc=0)

    plt.tight_layout()
    plt.savefig(plot_folder + plot_name + ".png")
    plt.show()


def create_data_plots(pickle_file, config_file):
    """
    Creates a bunch of plots using the given data
    :param pickle_file: The data the plot should be created for
    :param config_file: A config file describing that data
    """
    df = load_entire_df_pickle(pickle_file)

    config = json.load(open(config_file, "r"))

    df = df.set_index(config["date_column"])

    df = df.resample("W").mean()

    date_data = df.index.array

    for item in config["keep_columns"]:
        try:
            other_data = df[item]

            plt.plot(date_data, other_data)
            plt.xlabel("Date")
            plt.xticks(date_data, rotation=45)
            plt.gca().xaxis.set_major_locator(matplotlib.dates.MonthLocator())
            plt.gca().xaxis.set_major_formatter(matplotlib.dates.DateFormatter("%Y-%m"))
            plt.gcf().autofmt_xdate()
            column_name = config["column_names"][item]
            plt.ylabel(column_name)
            plotname = config["save_prefix"] + "_lineplot_" + ''.join(e for e in column_name if e.isalnum())
            plt.tight_layout()
            plt.savefig(plot_folder + plotname + ".png")
            plt.show()

        except KeyError:
            pass


if __name__ == "__main__":

    create_data_plots("bisamdata3.pkl", "bisambergconfig.json")
    create_data_plots("all2data3.pkl", "testconfig.json")

    # TODO Some of these plots appear in my thesis. Remove all this garbage
    """
    time_circle_secs("data5.pkl")
    time_circle_days("data5.pkl")
    scatter_pickle("data3.pkl", "tiltClearskyIrr", "PAC_SUM", "Clearsky Irradiation vs. Production")
    histogram_test("data5.pkl", "S09-VAISALA_Weatherstation: Temperature: Ambient [\u00b0C] (Mean) [\u00b0C] {p120601_c0901004001_0400022b}", "Ambient Temperature (C°)", plotname="temphisto")
    histogram_test("data3.pkl", "S09-VAISALA_Weatherstation: Temperature: Ambient [\u00b0C] (Mean) [\u00b0C] {p120601_c0901004001_0400022b}", "Ambient Temperature (C°)", plotname="temphisto_nothresh")
    histogram_test("data5.pkl", "Power Plant: Radiation Power (Mean) [W/m\u00b2] {p120601_plant_R}",
                   "Mean Radiation Power (W/m\u00b2)", plotname="radihisto")
    histogram_test("data3.pkl", "Power Plant: Radiation Power (Mean) [W/m\u00b2] {p120601_plant_R}", "Mean Radiation Power (W/m\u00b2)", plotname="radihisto_nothresh")
    histogram_test("data5.pkl", "INV.13.2: AC Active Power (Total) (Mean) [kW] {p120601_c0d01007004_07030f2d}", "AC Power (kW)", plotname="powerhisto")
    histogram_test("data3.pkl", "INV.13.2: AC Active Power (Total) (Mean) [kW] {p120601_c0d01007004_07030f2d}", "AC Power (kW)", plotname="powerhisto_nothresh")
    bar_and_line("data3.pkl", "INV.13.2: AC Active Power (Total) (Mean) [kW] {p120601_c0d01007004_07030f2d}", "AC Power (kW)",
                 "Power Plant: Radiation Power (Mean) [W/m\u00b2] {p120601_plant_R}", "Mean Radiation Power (W/m\u00b2)",
                 plotname="barlinetemprad_nothresh")
    bar_and_line("data5.pkl", "INV.13.2: AC Active Power (Total) (Mean) [kW] {p120601_c0d01007004_07030f2d}",
                 "AC Power (kW)",
                 "Power Plant: Radiation Power (Mean) [W/m\u00b2] {p120601_plant_R}",
                 "Mean Radiation Power (W/m\u00b2)",
                 plotname="barlinetemprad_nothresh"
                 
    histogram_test("data5.pkl",
                   "t_ext",
                   "Ambient Temperature (C°)", plotname="bisam_temphisto")
    histogram_test("data3.pkl",
                   "t_ext",
                   "Ambient Temperature (C°)", plotname="bisam_temphisto_nothresh")
    histogram_test("data5.pkl",
                   "g_irr",
                   "Mean Radiation Power (W/m\u00b2)", plotname="bisam_radihisto")
    histogram_test("data3.pkl",
                   "g_irr",
                   "Mean Radiation Power (W/m\u00b2)", plotname="bisam_radihisto_nothresh")
    histogram_test("data5.pkl",
                   "PAC_SUM",
                   "Sum of Inverter Productions (W)", plotname="bisam_powerhisto")
    histogram_test("data3.pkl",
                   "PAC_SUM",
                   "Sum of Inverter Productions (W)", plotname="bisam_powerhisto_nothresh")
    bar_and_line("bisamdata3.pkl",
                 "PAC_SUM",
                 "Sum of Inverter Productions (W)",
                 "g_irr",
                 "Mean Radiation Power (kW/m\u00b2)",
                 date_column="dt",
                 plotname="bisam_barlinetemprad_nothreshasdf")
    bar_and_line("data5.pkl",
                 "PAC_SUM",
                 "Sum of Inverter Productions (W)",
                 "g_irr",
                 "Mean Radiation Power (W/m\u00b2)",
                 date_column="dt",
                 plotname="bisam_barlinetemprad")
    contour_thing("../resources/imputed",
                  x_name='Power Plant: Radiation Power (Mean) [W/m²] {p120601_plant_R}',
                  y_name='Power Plant: Temperature: Module (Average) [°C] (Mean) [°C] {p120601_plant_Tm}',
                  color_name='Power Plant: DC Current (Inverter) (Mean) [A] {p120601_plant_T}',
                  output_file='firstthing.jpg',
                  title="I dunno what that is lul\n color is dc current though")
    """

