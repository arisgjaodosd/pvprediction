import json
import math
from random import sample

from pandas import DataFrame

from src.util import *

FUTURE_SUFFIX = "_future"
PAST_SUFFIX = "_past"
WRITE_COUNTER = 0


def reduce_nan_pickle(readfile, writefile, date_column_name=None, nan_row_threshold=1, nan_column_threshold=0.9):
    """
    Reads a csv file and writes the specified columns to a pickle file
    Also removes rows and columns with nan values according to the given thresholds

    :param nan_column_threshold: Percentage of allowed nan values in a column. If a column has less, it is removed
    :param nan_row_threshold: Percentage of allowed nan values in a row. If a row has less, it is removed.
        Row nan values are checked after columns have been removed
    :param readfile: the path of the file that should be read from
    :param writefile: the path of the file that should be written to
    :param date_column_name: name of the date column
    :return: None
    """

    chunk_size = 10000
    total_lines = 0
    nansum = None
    frames = None
    if readfile.endswith(".pkl"):
        frames = load_pickle_iterate(readfile)
    elif readfile.endswith(".dat") or readfile.endswith(".csv"):
        frames = load_csv(readfile, date_column_name, chunk_size)
    try:
        for chunk in frames:
            nanamounts = chunk.isna().sum()
            if nansum is None:
                nansum = nanamounts
            else:
                nansum = nansum + nanamounts

            total_lines = total_lines + chunk.shape[0]
    except EOFError:
        # End of file is reached, do nothing
        pass

    nan_col_thresh_amount = total_lines * (1 - nan_column_threshold)
    drop_columns = []
    for i in range(0, nansum.size):
        if nansum[i] > nan_col_thresh_amount:
            drop_columns.append(i)
    print("dropping the following columns:")
    print(drop_columns)
    keep_columns_amount = len(nansum) - len(drop_columns)

    total_rows = 0

    clear_file(writefile)

    if readfile.endswith(".pkl"):
        frames = load_pickle_iterate(readfile)
    elif readfile.endswith(".dat") or readfile.endswith(".csv"):
        frames = load_csv(readfile, date_column_name, chunk_size)

    try:
        for chunk in frames:
            # Drop columns and rows if they have too many nan values
            chunk.drop(chunk.columns[drop_columns], axis=1, inplace=True)
            chunk.dropna(thresh=keep_columns_amount * nan_row_threshold, inplace=True)
            total_rows = total_rows + chunk.shape[0]
            append_to_pickle(chunk, writefile)
    except EOFError:
        pass


def shift_by_minutes(readfile, writefile, date_column, shift_features, shift_minutes=60, shift_margin=15):
    """
    Adds a column for the production from a certain amount of hours in the future
    Finds for each row the row that is :hours hours in the future and uses the production of that row
    :param readfile: the file path of the dataframe that should be read
    :param writefile: the file path that the results should be written to
    :param date_column: the name of the column in the dataframe that contains the time
    :param shift_features: features to search on other time stamps and put in the same line
    :param shift_minutes: the amount of time in hours that the features should be away
    :param shift_margin: the amount of time in minutes that the feature timestamps can be off of shift_minutes
    :return: None
    """
    df = load_entire_df_pickle(readfile)
    to_shift_df = df[shift_features].values
    future_data = []
    dates = df[date_column]
    future_index = 0
    date_values = dates.values
    df.index = range(0, df.index.size)

    for i in range(0, len(date_values)):
        curr_date = date_values[i]
        try:
            while date_values[future_index] - curr_date < np.timedelta64(shift_minutes - shift_margin, 'm'):
                future_index = future_index + 1
            while curr_date - date_values[future_index] > np.timedelta64(shift_minutes + shift_margin, 'm'):
                future_index = future_index + 1
            if abs((date_values[future_index - 1] - curr_date - np.timedelta64(shift_minutes, 'm')) /
                   np.timedelta64(1, 'm')) < \
                    abs((date_values[future_index] - curr_date - np.timedelta64(shift_minutes, 'm')) /
                        np.timedelta64(1, 'm')):
                future_index = future_index - 1
            if np.timedelta64(shift_minutes - shift_margin, 'm') < \
                    date_values[future_index] - curr_date < \
                    np.timedelta64(shift_minutes + shift_margin, 'm'):
                future_data.append(to_shift_df[future_index])
            else:
                df = df.drop(i)
            future_index = i

        except IndexError:
            df = df.drop(i)

    df.index = range(0, df.index.size)

    future_names = [item + FUTURE_SUFFIX for item in shift_features]
    future_df = DataFrame(future_data, columns=future_names)
    df = df.join(future_df)
    save_as_pickle(df, writefile)


def shift_by_minutes_reverse(readfile, writefile, date_column, shift_features, shift_minutes=60, shift_margin=15):
    """
    Adds a column for the production from a certain amount of hours in the future
    Finds for each row the row that is :hours hours in the future and uses the production of that row
    :param shift_features: features
    :param readfile: the file path of the dataframe that should be read
    :param writefile: the file path that should be written to
    :param date_column: the name of the column in the dataframe that contains the time
    :param shift_minutes: the amount of time in hours that the future production should be away
    :return: None
    """
    df = load_entire_df_pickle(readfile)
    to_shift_df = df[shift_features].values
    past_data = []
    dates = df[date_column]
    date_values = dates.values
    past_index = len(date_values) - 1
    df.index = range(0, df.index.size)
    shift_timedelta = np.timedelta64(shift_minutes, 'm')

    for i in list(reversed(range(0, len(date_values)))):
        curr_date = date_values[i]
        try:
            while curr_date - date_values[past_index] <= np.timedelta64(shift_minutes - shift_margin, 'm'):
                past_index = past_index - 1
            if np.timedelta64(shift_minutes - shift_margin, 'm') <= curr_date - date_values[past_index] <= \
                    np.timedelta64(shift_minutes + shift_margin, 'm'):
                while abs((curr_date - date_values[past_index - 1]) - shift_timedelta) <= \
                        abs((curr_date - date_values[past_index]) - shift_timedelta):
                    past_index = past_index - 1
                past_data.append(to_shift_df[past_index])
            else:
                df = df.drop(i)
            past_index = i
        except IndexError:
            df = df.drop(i)

    df.index = range(0, df.index.size)

    future_names = [item + PAST_SUFFIX for item in shift_features]
    future_df = DataFrame(reversed(past_data), columns=future_names)

    df = df.join(future_df)
    save_as_pickle(df, writefile)


def shorten_data(readfile, writefile, keep_percentage=20):
    """
    Cuts random rows from the dataframe so that only a certain percentage of the data is left
    :param readfile: the file path of the dataframe that should be read
    :param writefile: the file path that should be written to
    :param keep_percentage: the percentage of the data that should be kept
    :return: None
    """
    df = load_entire_df_pickle(readfile)
    data_amount = df.shape[0]
    keep_indices = sample(range(data_amount), int(data_amount * (keep_percentage / 100)))
    shortened_data = df.loc[keep_indices, :]
    newindex = range(0, shortened_data.index.size)
    shortened_data.index = newindex

    save_as_pickle(shortened_data, writefile)


def impute_pickle(readfile, writefile):
    """
    Reads a pickle file and imputes all nan values. Nan values at the beginning and end of the file are cut

    :param readfile: the path of the file that should be read from
    :param writefile: the path of the file that should be written to
    :return: None
    """
    df = load_entire_df_pickle(readfile)
    newdf = df.interpolate()

    newdf.dropna(inplace=True)
    newindex = range(0, newdf.index.size)
    newdf.index = newindex
    save_as_pickle(newdf, writefile)


def shift_pickle(readfile, writefile, column_to_shift, lines=5):
    """
    Reads a pickle file containing pv data and shifts it so that the energy production is aligned with the rest of the
    data from a certain amount of hours ago

    :param lines: number of lines that should be shifted
    :param column_to_shift: name of the column that should be shifted
    :param readfile: the path of the file that should be read from
    :param writefile: the path of the file that should be written to
    :return: None
    """
    df = load_entire_df_pickle(readfile)
    to_shift = df[column_to_shift]

    shifted = to_shift.values[lines:]

    df.pop(column_to_shift)
    df[column_to_shift] = DataFrame(shifted)

    save_as_pickle(df[:-lines], writefile)


def filter_by_radiation(read_path, write_path, radiation_column, lower_threshold=None, upper_threshold=None):
    """
    Filters the dataframe by radiation, only keeping values within the specified thresholds
    :param read_path: the file path of the dataframe that should be read
    :param write_path: the file path that should be written to
    :param radiation_column: the name of the radiation column
    :param lower_threshold: values below this value will be removed. Can be None for no limit
    :param upper_threshold: values above this value will be removed. Can be None for no limit
    :return: None
    """
    df = load_entire_df_pickle(read_path)
    if lower_threshold is None:
        new_df = df[df[radiation_column].lt(upper_threshold)]
    elif upper_threshold is None:
        new_df = df[df[radiation_column].gt(lower_threshold)]
    else:
        new_df = df[df[radiation_column].between(lower_threshold, upper_threshold)]

    save_as_pickle(new_df, write_path)


def list_select_features(read_path, write_path, keep_features):
    """
    Filter dataframe so that only the given features remain
    :param read_path: the file path of the dataframe that should be read
    :param write_path: the file path that should be written to
    :param keep_features: list of feature names that should be kept
    :return: None
    """
    df = load_entire_df_pickle(read_path)

    df = df[keep_features]

    # TODO: Make this function adapt to the config file
    # make production values in all2 from kw to w
    kw_column = "INV.13.2: AC Active Power (Total) (Mean) [kW] {p120601_c0d01007004_07030f2d}"
    if kw_column in df:
        df[kw_column] = df[kw_column] * 1000

    save_as_pickle(df, write_path)


def split_train_test(read_path, train_write_path, test_write_path, train_percentage=20):
    """
    :param read_path: the file path of the dataframe that should be read
    :param train_write_path: the file path that the odd days should be written to
    :param test_write_path: the file path that the even days should be written to
    :param train_percentage: percentage of the data to be used for training. Rest is used for testing
    :return: None
    """
    df = load_entire_df_pickle(read_path)
    df_size = df.shape[0]

    if train_percentage <= 50:
        nth = 100.0/train_percentage
        indices = np.linspace(0, df_size - 1, int(df_size/nth)).astype(int)
        train_series = df.iloc[indices, :]
        test_series = df.drop(indices)
    else:
        nth = 100.0 / (100 - train_percentage)
        indices = np.linspace(0, df_size - 1, int(df_size / nth)).astype(int)
        print(indices)
        test_series = df.iloc[indices, :]
        train_series = df.drop(indices)

    save_as_pickle(train_series, train_write_path)
    save_as_pickle(test_series, test_write_path)


def set_negatives_to_zero(read_path, write_path, features):
    """
    Sets negative values of given columns to 0
    :param read_path: the file path of the dataframe that should be read
    :param write_path: the file path that should be written to
    :param features: the features that should be set to 0
    :return: None
    """
    df = load_entire_df_pickle(read_path)
    for feature in features:
        df.loc[(df[feature] < 0), feature] = 0

    save_as_pickle(df, write_path)


def time_to_cycle(read_path, write_path, time_column_name):
    """
    Adds features for sin/cos of seconds in day and day in year to the dataframe
    These can be used in machine learning models to represent the cyclic nature of days and years
    Names of added features: ['sin_seconds', 'cos_seconds', 'sin_days', 'cos_days']
    :param read_path: the file path of the dataframe that should be read
    :param write_path: the file path that should be written to
    :param time_column_name: name of the column that contains the dates
    :return: None
    """
    df = load_entire_df_pickle(read_path)

    total_days_in_year = 366
    total_seconds_in_day = 24 * 60 * 60

    day_in_year = pandas.Series(float(item.day_of_year) for item in df[time_column_name])
    second_in_day = pandas.Series(float(item.second + item.minute * 60 + item.hour * 3600)
                                  for item in df[time_column_name])

    new_index = range(0, df.index.size)
    df.index = new_index

    df['sin_seconds'] = np.sin(2 * np.pi * second_in_day / total_seconds_in_day)
    df['cos_seconds'] = np.cos(2 * np.pi * second_in_day / total_seconds_in_day)

    df['sin_days'] = np.sin(2 * np.pi * day_in_year / total_days_in_year)
    df['cos_days'] = np.cos(2 * np.pi * day_in_year / total_days_in_year)

    save_as_pickle(df, write_path)


def add_stability(read_path, write_path, date_column_name, radiation_column_name):
    """
    Adds stability from qickAndDirtyStationarity to the dataframe
    :param read_path: the file path of the dataframe that should be read
    :param write_path: the file path that should be written to
    :param date_column_name: name of the column that contains the dates
    :param radiation_column_name: name of the column that contains the radiation values
    :return: None
    """

    df = load_entire_df_pickle(read_path)

    stability_list = qickAndDirtyStationarity(df[date_column_name], df[radiation_column_name])
    df["stability"] = stability_list

    save_as_pickle(df, write_path)


def qickAndDirtyStationarity(times, P, badlimit=0.25):
    """
    Source: Code from Bernhard Kubicek
    I know there's a typo in the function name and it's not snake case, but I wanted to stay true to the source :^)

    this calculates, if the data P at the datetimes times did quickly change in time
    if the second derivate of P is larger than badlimit*p[center], its consdiered instationary
    returns 0 if totally unstationary, and 1 if stationary

    make the second derivation of P, divide by badlimit, clip that so its in [0, 1]
    (not scaled, larger values are set to 1), and spread instability sideways a bit by using the average
    of a point and a neighbour in case the neighbour is more instable
    return 1 - that list, in order to set 0 to unstable and 1 to stable (the list is in [0, 1], so we gucci

    :param times:
    :param P:
    :param badlimit:
    :return:
    """
    n = min(len(times), len(P))
    c = np.ones(n)  # preallocate with all good=1, logic is inversed in return statement
    for i in range(n - 1):
        if i == 0:
            continue
        d2 = 0
        if P[i - 1] is not None and \
                P[i + 1] is not None and \
                P[i] is not None:
            #2nd derivation of P
            d2 = P[i - 1] + P[i + 1] - 2 * P[i]

        if P[i] == 0:
            continue
        badchange = badlimit * P[i]
        badness = np.clip(math.fabs(d2) / badchange, 0, 1)
        c[i] = badness

    d = c

    # additionally let any badness spread sidewards
    for gg in range(2):
        for i in range(n - 1):
            if i == 0:
                continue
            if not math.isnan(d[i - 1]):
                d[i] = max((d[i] + d[i - 1]) * 0.5, d[i])
            if not math.isnan(d[i + 1]):
                d[i] = max((d[i] + d[i + 1]) * 0.5, d[i])

    return 1 - d


def prepare_full_suite(config_file):
    """
    Does a set of preprocessing steps for proper learning and writes the resulting dataframe to a
    pickle file. All relevant parameters for that must be set in a config file
    After every step, the resulting data is written to a pickle file
    :param config_file: the file that contains the configurations for the dataset
    :return: None
    """

    config = json.load(open(config_file, "r"))

    time_column_name = config["date_column"]
    radiation_column_name = config["radiation_column"]
    shift_config = config["shift_config"]

    prefix = config["save_prefix"]
    write_counter = [0]

    write_path_old, write_path_new = inc_write_path(config["save_prefix"], write_counter)

    reduce_nan_pickle(config["read_file"], write_path_new, time_column_name)
    print("Filtering nan values done!")

    write_path_old, write_path_new = inc_write_path(config["save_prefix"], write_counter)

    list_select_features(write_path_old, write_path_new, config["keep_columns"])
    print("Filtering features done!")

    write_path_old, write_path_new = inc_write_path(config["save_prefix"], write_counter)

    set_negatives_to_zero(write_path_old, write_path_new, [radiation_column_name, config["dv_column"]])
    print("Setting negative values to zero done!")

    write_path_old, write_path_new = inc_write_path(config["save_prefix"], write_counter)

    shift_by_minutes_reverse(write_path_old, write_path_new, time_column_name, shift_config["features"],
                             shift_config["time_delta_minutes"],
                             shift_config["time_delta_threshold"])
    print("Shifting done!")

    write_path_old, write_path_new = inc_write_path(config["save_prefix"], write_counter)

    filter_by_radiation(write_path_old, write_path_new, radiation_column_name,
                        lower_threshold=config.get("min_radiation"), upper_threshold=config.get("max_radiation"))
    print("Radiation filtering done!")

    write_path_old, write_path_new = inc_write_path(config["save_prefix"], write_counter)

    time_to_cycle(write_path_old, write_path_new, time_column_name)
    print("Making time circular done!")

    write_path_old, write_path_new = inc_write_path(config["save_prefix"], write_counter)

    add_stability(write_path_old, write_path_new, time_column_name, radiation_column_name)
    print("Adding stability done!")

    write_path_old, write_path_new = inc_write_path(config["save_prefix"], write_counter)

    write_path_train = prefix + "_train.pkl"
    write_path_test = prefix + "_test.pkl"

    split_train_test(write_path_old, write_path_train, write_path_test)
    print("Splitting by day done!")


def inc_write_path(prefix, counter):
    """
    Updates the string for the file paths used for reading and writing of data
    :param prefix: prefix to add to the start of the file names
    :param counter: counter to increment (must be a list where the first value is the counter, updates are not saved on
        numbers)
    """
    write_path_old = prefix + "data" + str(counter[0]) + ".pkl"
    counter[0] += 1
    write_path_new = prefix + "data" + str(counter[0]) + ".pkl"
    return write_path_old, write_path_new


if __name__ == "__main__":
    prepare_full_suite(config_file="bisambergconfig.json")
    #prepare_full_suite(config_file="testconfig.json")
