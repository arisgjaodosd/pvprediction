import json
from datetime import date

import pandas as pd
from scipy.stats import pearsonr, spearmanr
from sklearn import linear_model, svm, tree
from sklearn.covariance import EllipticEnvelope
from sklearn.ensemble import IsolationForest
from sklearn.metrics import *
from sklearn.neural_network import MLPRegressor
from sklearn.svm import OneClassSVM
from sklearn.tree import DecisionTreeRegressor

from src import Plotter
from src.util import *

FUTURE_SUFFIX = "_future"
PAST_SUFFIX = "_past"


class Predictor:
    """
    Class representing a dataset. Multiple different predictors can be taught.
    """
    log_name = "log.csv"
    train_x = None
    train_y = None
    test_x = None
    test_y = None

    train_x_min = None
    train_y_min = None
    test_x_min = None
    test_y_min = None

    train_x_max = None
    train_y_max = None
    test_x_max = None
    test_y_max = None

    dates = None
    dv_name = 'Unnamed'
    log_suffix = ''
    filename_train = ""  # "train_data.pkl"
    filename_test = ""  # "test_data.pkl"
    before_time = datetime.now()

    prefix = ""

    train_rain = None
    test_rain = None

    config = None

    def __init__(self, config_file, log_name):
        """
        Sets some values to be reused by multiple regression models for a data set
        :param config_file: The config file containing the save prefix
        :param log_name: The name that the log file should have for the calculated metrics
        """
        config = json.load(open(config_file, "r"))
        self.prefix = config["save_prefix"]
        self.filename_train = self.prefix + "_train.pkl"
        self.filename_test = self.prefix + "_test.pkl"
        self.log_name = log_name
        self.prepare_data(config)
        self.config = config

    def multiple_regression(self):
        """
        Teaches a linear regression model with the given data and calculates metrics
        :return: the taught regression model
        """
        regr = linear_model.LinearRegression()
        predicted_y = regr.predict(self.test_x)
        expected_y = self.test_y.to_list()

        formula = ''

        for item in range(0, regr.coef_.size):
            formula += "%d * " % regr.coef_[item]
            formula += "%s + \n" % self.train_x.columns[item]

        formula += "%d" % regr.intercept_

        print("formula for multiple regression ")
        print(formula)

        self.calculate_metrics(param_amount=self.test_x.shape[1],
                               expected=expected_y,
                               predicted=predicted_y,
                               title="LR")

        return regr

    def mlp_regression(self, layers: tuple = (100), solver=None):
        """
        Teaches a multilayer perceptron model with the given data and calculates metrics
        :param layers: The layers to be used for the perceptron. Must be a tuple of numbers
            Each value in the tuple represents the size of one layer
        :param solver: The solver to be used by the perceptron. See documentation of MLPRegressor
        :return: the taught regression model
        """
        if solver is None:
            for solver in ["lbfgs", "adam"]:
                self.mlp_regression(layers, solver)
        else:
            regr = MLPRegressor(random_state=69, max_iter=700, solver=solver, hidden_layer_sizes=layers).fit(
                self.train_x, self.train_y)
            predicted_y = regr.predict(self.test_x)
            expected_y = self.test_y.to_list()
            if np.isnan(predicted_y).any():
                print("Model creation did not work with solver %s" % solver)
            else:
                self.calculate_metrics(param_amount=self.test_x.shape[1],
                                       expected=expected_y,
                                       predicted=predicted_y,
                                       title="MPR, L=%s, S=%s" % (
                                           layers.__str__(), solver))

    def tree_regression(self, depths=[5, 10], save_graph=False):
        """
        Teaches regression trees with the given data and calculates metrics
        :param depths: The maximum depth values that trees should be created for. Must be a list of numbers
        :param save_graph: Whether a graph of the tree should be saved as a png
        :return: the taught regression model
        """
        for depth in depths:
            regrtree = DecisionTreeRegressor(max_depth=depth)
            regrtree.fit(self.train_x, self.train_y)

            predicted_y = regrtree.predict(self.test_x)

            expected_y = self.test_y.to_list()
            self.calculate_metrics(param_amount=self.test_x.shape[1],
                                   expected=expected_y,
                                   predicted=predicted_y,
                                   title="RT, depth=%i" % (depth))

            if save_graph:
                import pydotplus
                tree.export_graphviz(regrtree, out_file="dotdata%d" % depth, feature_names=self.train_x.columns)
                graph = pydotplus.graph_from_dot_file("dotdata%d" % depth)
                graph.write_png('my_decision_tree%d.png' % depth)

    def calculate_metrics(self, param_amount, expected, predicted, title, sigma=0.2, print_results=True,
                          log_results=True, log_shortened=True):
        """
        Calculates some metrics to rate machine learning models and prints them
        :param param_amount: the amount of independent values used by the model
        :param expected: the expected output values
        :param predicted: the predicted output values
        :param title: title or short description of the used model
        :param sigma: the sigma value used for aic and bic
        :return: None
        """
        r2 = r2_score(expected, predicted)
        log_likelihood = self.calculate_log_likelihood(expected, predicted, sigma)
        aic = self.calculate_aic(param_amount, log_likelihood)
        bic = self.calculate_bic(param_amount, len(expected), log_likelihood)
        max_error_value = max_error(expected, predicted)
        mean_absolute_error_value = mean_absolute_error(expected, predicted)

        mean_relative_error = mean_absolute_error(expected, predicted) / np.mean(expected)
        mean_squared_error_value = mean_squared_error(expected, predicted)
        root_mean_squared_error_value = mean_squared_error(expected, predicted, squared=False)
        median_absolute_error_value = median_absolute_error(expected, predicted)
        precent_root_mean_squared_error_value = root_mean_squared_error_value / max(expected)

        two_percent_hit = self.calculate_close_hits(expected, predicted, 0.02)
        three_percent_hit = self.calculate_close_hits(expected, predicted, 0.03)
        five_percent_hit = self.calculate_close_hits(expected, predicted, 0.05)
        ten_percent_hit = self.calculate_close_hits(expected, predicted, 0.10)

        two_percent_hit_threshold = self.calculate_close_hits(expected, predicted, 0.02, 50)
        three_percent_hit_threshold = self.calculate_close_hits(expected, predicted, 0.03, 50)
        five_percent_hit_threshold = self.calculate_close_hits(expected, predicted, 0.05, 50)
        ten_percent_hit_threshold = self.calculate_close_hits(expected, predicted, 0.10, 50)

        pearson_corr = pearsonr(expected, predicted)[0]
        spearman_rank = spearmanr(expected, predicted)[0]

        after_time = datetime.now()
        runtime = after_time - self.before_time

        title = title + " " + self.log_suffix

        Plotter.create_all_plots(predicted, expected, self.dates, title="")

        if log_results:
            if log_shortened:
                log_line_shortened = [title,
                                      r2,
                                      pearson_corr,
                                      spearman_rank,
                                      max_error_value,
                                      mean_absolute_error_value,
                                      mean_relative_error,
                                      mean_squared_error_value,
                                      root_mean_squared_error_value,
                                      precent_root_mean_squared_error_value,
                                      median_absolute_error_value,
                                      two_percent_hit,
                                      three_percent_hit,
                                      five_percent_hit,
                                      ten_percent_hit]

                log_predictor_result_shortened(log_line_shortened, self.log_name)
            else:
                log_line = [title,
                            r2,
                            pearson_corr,
                            spearman_rank,
                            log_likelihood,
                            aic,
                            bic,
                            max_error_value,
                            mean_absolute_error_value,
                            mean_relative_error,
                            mean_squared_error_value,
                            root_mean_squared_error_value,
                            precent_root_mean_squared_error_value,
                            median_absolute_error_value,
                            two_percent_hit,
                            three_percent_hit,
                            five_percent_hit,
                            ten_percent_hit,
                            two_percent_hit_threshold,
                            three_percent_hit_threshold,
                            five_percent_hit_threshold,
                            ten_percent_hit_threshold,
                            runtime]

                log_predictor_result(log_line, self.log_name)

        if print_results:
            print("Metrics for %s" % title)
            print("")
            print("R2 Score: %f" % r2)
            print("AIC: %f" % aic)
            print("BIC: %f" % bic)
            print("Max error: %f" % max_error_value)
            print("Mean absolute error: %f" % mean_absolute_error_value)
            print("Mean squared error: %f" % mean_squared_error_value)
            print("Root mean squared error: %f" % root_mean_squared_error_value)
            print("Median absolute error: %f" % median_absolute_error_value)
            print("Pearson Correlation Coefficient: %f" % pearson_corr)
            print("Spearman Rank correlation: %f" % spearman_rank)
            print("Percentage of predictions within 2" + "%" + " of actual value: %f" % two_percent_hit)
            print("Percentage of predictions within 3" + "%" + " of actual value: %f" % three_percent_hit)
            print("Percentage of predictions within 5" + "%" + " of actual value: %f" % five_percent_hit)
            print("Percentage of predictions within 10" + "%" + " of actual value: %f" % ten_percent_hit)
            print(
                "Percentage of predictions within 2" + "%" + " of values above 10000A: %f" % two_percent_hit_threshold)
            print(
                "Percentage of predictions within 3" + "%" + " of values above 10000A: %f" % three_percent_hit_threshold)
            print(
                "Percentage of predictions within 5" + "%" + " of values above 10000A: %f" % five_percent_hit_threshold)
            print(
                "Percentage of predictions within 10" + "%" + " of values above 10000A: %f" % ten_percent_hit_threshold)
            print("")
            print("")

        self.before_time = datetime.now()

    def calculate_close_hits(self, expected, predicted, margin, threshold=0):
        """
        Calculates the percentage of predicted values that are within a certain margin of expected values
        :param expected: excpected output values
        :param predicted: predicted output values
        :param margin: margin that determines which values are counted. percentage of the expected value
        :param threshold: values below this threshold are not counted
        :return: percentage of close hits
        """
        hits = 0
        amount = 0
        for i in range(0, len(expected)):
            if expected[i] > threshold:
                amount += 1
                if expected[i] * (1 - margin) < predicted[i] < expected[i] * (1 + margin):
                    hits += 1

        return float(hits) / float(amount)

    def calculate_log_likelihood(self, expected, predicted, sigma):
        """
        Calculates the log likelihood value needed for aic and bic
        :param expected: excpected output values
        :param predicted: predicted output values
        :param sigma: variance used in the formula
        :return: the calculated log likelihood
        """
        deriv_sum = 0
        for i in range(0, len(expected)):
            deriv_sum += (predicted[i] - expected[i]) ** 2

        log_likelihood = (- len(expected) / 2) * np.log(2 * np.pi * (sigma ** 2)) - 1 / (2 * sigma ** 2) * deriv_sum
        return log_likelihood

    def calculate_aic(self, param_amount, log_likelihood):
        """
        Calculates the metric AIC (Akaike Information Criterion)
        :param param_amount: the amount of independent values used in the model
        :param log_likelihood: the log likelihood value
        :return: the calculated AIC
        """
        aic = 2 * param_amount - 2 * log_likelihood
        return aic

    def calculate_bic(self, param_amount, set_size, log_likelihood):
        """
            Calculates the metric BIC (Bayesian Information Criterion)
            :param param_amount: the amount of independent values used in the model
            :param log_likelihood: the log likelihood value
            :return: the calculated BIC
        """
        bic = param_amount * np.log(set_size) - 2 * log_likelihood
        return bic

    def prepare_data(self, config, normalize=True):

        train_df = load_entire_df_pickle(self.filename_train)
        test_df = load_entire_df_pickle(self.filename_test)

        if "Power Plant: DC Current (Inverter) (Mean) [A] {p120601_plant_T}" in train_df:
            train_df = train_df.drop("Power Plant: DC Current (Inverter) (Mean) [A] {p120601_plant_T}", axis=1)
            train_df = train_df.drop("Power Plant: DC Current (Inverter) (Mean) [A] {p120601_plant_T}_past", axis=1)
            test_df = test_df.drop("Power Plant: DC Current (Inverter) (Mean) [A] {p120601_plant_T}", axis=1)
            test_df = test_df.drop("Power Plant: DC Current (Inverter) (Mean) [A] {p120601_plant_T}_past", axis=1)

        self.dv_name = config["dv_column"]
        ivs = train_df.columns.drop(self.dv_name).drop(config["date_column"]).drop(config["dv_column"] + PAST_SUFFIX)

        self.train_x = train_df[ivs]
        self.test_x = test_df[ivs]
        self.train_y = train_df[self.dv_name]
        self.test_y = test_df[self.dv_name]
        self.dates = test_df[config["date_column"]]

        if normalize:
            self.train_x, self.test_x = self.normalize_data(self.train_x, self.test_x)

    def normalize_data(self, df1, df2):
        combined = pd.concat([df1, df2])

        return (df1 - combined.min()) / (combined.max() - combined.min()), \
               (df2 - combined.min()) / (combined.max() - combined.min())

    def svm_regression(self, kernel=None, epsilon=None):
        """
        Teaches a support vector machine regression model with the given data and calculates metrics
        Uses four different kernels
        Very slow for large files
        :return: the taught svm regression model
        """

        if kernel is None:
            def_kernels = ["linear", "poly", "rbf"]
            for def_kernel in def_kernels:
                self.svm_regression(def_kernel, epsilon)
            return

        if epsilon is None:
            def_epsilons = [0.001, 0.01, 0.1, 1]
            for def_epsilon in def_epsilons:
                self.svm_regression(kernel, def_epsilon)
            return

        regr = svm.SVR(kernel=kernel, epsilon=epsilon)
        regr.fit(self.train_x, self.train_y)
        predicted_y = regr.predict(self.test_x)
        expected_y = self.test_y.to_list()
        self.calculate_metrics(param_amount=self.test_x.shape[1],
                               expected=expected_y,
                               predicted=predicted_y,
                               title="SVR, K=%s, epsilon=%f" % (kernel, epsilon))

        if kernel == "linear":
            formula = ''

            print(regr.coef_)
            print(regr.intercept_)
            print("Formula for svm with linear kernel and epsilon = %d" % epsilon)
            for item in range(0, regr.coef_.size):
                formula += "%d * " % regr.coef_[0][item]
                formula += "%s + \n" % self.train_x.columns[item]

            formula += "%d" % regr.intercept_

            print(formula)

    def guess_average(self):
        """
        A model that always guesses the average production. Any reasonable model should be better than this
        """
        predicted_y = [self.train_y.mean()] * len(self.test_y)
        expected_y = self.test_y.to_list()
        self.calculate_metrics(param_amount=self.test_x.shape[1],
                               expected=expected_y,
                               predicted=predicted_y,
                               title="Always guess average production")

    def all_regressions(self):
        """
        Tries some regression models using the given data.
        :return: None
        """
        self.before_time = datetime.now()
        self.multiple_regression()
        self.mlp_regression((20))
        self.mlp_regression((50))
        self.mlp_regression((100))
        self.mlp_regression((200))
        self.mlp_regression((50, 30))
        self.mlp_regression((100, 10))
        self.mlp_regression((100, 30))
        self.mlp_regression((50, 30, 20))
        self.mlp_regression((100, 30, 20))
        self.mlp_regression((30, 20, 10, 10))
        self.mlp_regression((50, 40, 30, 20, 10))
        self.mlp_regression((50, 40, 30, 20, 10, 10))
        self.mlp_regression((50, 40, 30, 20, 10, 10, 10))
        self.tree_regression(depths=list(range(2, 20, 2)))
        self.svm_regression()

    def elite_regression(self, title=''):
        """
        Tries some regression models that turned out to be really good.
        :return: None
        """
        self.before_time = datetime.now()

        log_line_shortened = [title,
                              '',
                              '',
                              '',
                              '',
                              '',
                              '',
                              '',
                              '',
                              '',
                              '',
                              '',
                              '',
                              '',
                              '']

        log_predictor_result_shortened(log_line_shortened, self.log_name)

        self.multiple_regression()
        self.mlp_regression((50, 40, 30, 20, 10, 10), solver='lbfgs')
        self.tree_regression(depths=[5, 10])
        self.svm_regression(kernel='poly', epsilon=0.1)

    def filter_outliers_forest(self, contamination=0.1):
        iso = IsolationForest(contamination=contamination)
        yhat = iso.fit_predict(self.train_x)
        mask = yhat != -1
        self.train_x, self.train_y = self.train_x[mask], self.train_y[mask]

    def filter_outliers_svm(self):
        ee = OneClassSVM(nu=0.1)
        yhat = ee.fit_predict(self.train_x)

        mask = yhat != -1
        self.train_x, self.train_y = self.train_x[mask], self.train_y[mask]

    def filter_outliers_covariance(self):
        ee = EllipticEnvelope(contamination=0.1)
        yhat = ee.fit_predict(self.train_x)

        mask = yhat != -1
        self.train_x, self.train_y = self.train_x[mask], self.train_y[mask]
        return self.train_x, self.train_y

    def exclude_each_feature(self):

        past_features = [feat for feat in self.train_x.columns.values if "past" in feat]
        time_features = ["sin_seconds", "cos_seconds", "sin_days", "cos_days"]
        sun_pos_features = self.config['sun_pos_features']

        # use only radiation
        old_train_x = self.train_x
        old_test_x = self.test_x
        self.train_x = self.train_x[[self.config['radiation_column']]]
        self.test_x = self.test_x[[self.config['radiation_column']]]
        self.elite_regression(title="Only Radiation")
        self.train_x = old_train_x
        self.test_x = old_test_x

        # exclude each feature once
        for iv in self.train_x.columns.values:
            if iv in self.config["column_names"]:
                old_train_x = self.train_x
                old_test_x = self.test_x
                self.train_x = self.train_x.drop(iv, axis=1)
                self.test_x = self.test_x.drop(iv, axis=1)
                column_name = self.config["column_names"][iv]
                self.elite_regression(title="Without " + column_name)
                self.train_x = old_train_x
                self.test_x = old_test_x

        # exclude all time features
        old_train_x = self.train_x
        old_test_x = self.test_x
        self.train_x = self.train_x.drop(time_features, axis=1)
        self.test_x = self.test_x.drop(time_features, axis=1)
        self.elite_regression(title="No Time Features")
        self.train_x = old_train_x
        self.test_x = old_test_x

        # exclude all past features
        old_train_x = self.train_x
        old_test_x = self.test_x
        self.train_x = self.train_x.drop(past_features, axis=1)
        self.test_x = self.test_x.drop(past_features, axis=1)
        self.elite_regression(title="No Past Features")
        self.train_x = old_train_x
        self.test_x = old_test_x

        # use only past features
        old_train_x = self.train_x
        old_test_x = self.test_x
        self.train_x = self.train_x[past_features]
        self.test_x = self.test_x[past_features]
        self.elite_regression(title="Only Features 1h before")
        self.train_x = old_train_x
        self.test_x = old_test_x

        # use time and sunposition features
        old_train_x = self.train_x
        old_test_x = self.test_x
        self.train_x = self.train_x[sun_pos_features + time_features]
        self.test_x = self.test_x[sun_pos_features + time_features]
        self.elite_regression(title="Only Time and Sun Position")
        self.train_x = old_train_x
        self.test_x = old_test_x

        # use time, sun position and past features
        old_train_x = self.train_x
        old_test_x = self.test_x
        self.train_x = self.train_x[sun_pos_features + time_features + past_features]
        self.test_x = self.test_x[sun_pos_features + time_features + past_features]
        self.elite_regression(title="Only Time, Sun Position, Past Features")
        self.train_x = old_train_x
        self.test_x = old_test_x


if __name__ == "__main__":
    today = date.today()
    d1 = today.strftime("%Y-%m-%d")

    log_file = "log" + d1 + ".csv"

    # config_file = "bisambergconfig.json"
    config_file = "testconfig.json"

    predictor = Predictor(config_file, log_file)
    log_predictor_result_shortened(
        [predictor.config["save_prefix"], '', '', '', '', '', '', '', '', '', '', '', '', '', ''], predictor.log_name)

    # predictor.guess_average()
    predictor.elite_regression()
    predictor.exclude_each_feature()
    #predictor.all_regressions()
