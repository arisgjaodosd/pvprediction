import gzip
from datetime import datetime
# import pickle5 as pickle
import pickle
from pathlib import Path

import pandas
import csv
import os
import numpy as np

csv_header = ["time",
              "model",
              "r2",
              "pearson_corr",
              "spearman_rank",
              "log_likelihood",
              "aic",
              "bic",
              "max_error_value",
              "mean_absolute_error_value",
              "mean_squared_error_value",
              "root_mean_squared_error_value",
              "median_absolute_error_value",
              "two_percent_hit",
              "three_percent_hit",
              "five_percent_hit",
              "ten_percent_hit",
              "two_percent_hit_threshold",
              "three_percent_hit_threshold",
              "five_percent_hit_threshold",
              "ten_percent_hit_threshold",
              "run duration"]

csv_header_shortened = [
    "Model",
    "R2",
    "PC",
    "SR",
    "maxAE",
    "meanAE",
    "MRE",
    "MSE",
    "RMSE",
    "pRMSE",
    "medAE"
    "2PH",
    "3PH",
    "5PH",
    "10PH"]

log_folder = Path("../log")

plot_folder = "../plots/"

data_folder = Path("../resources")


def write_df(filename, dataframe, use_zip=False):
    if use_zip:
        pfile = gzip.open(filename + ".gz", "wb")
    else:
        print("zip pls")
    pickle.dump(dataframe, pfile, protocol=pickle.HIGHEST_PROTOCOL)
    pfile.close()


def save_as_pickle(to_pickle, filename):
    with open(data_folder / filename, 'wb') as file:
        pickle.dump(to_pickle, file)


def clear_file(filename):
    try:
        os.remove(data_folder / filename)
    except FileNotFoundError:
        pass


def append_to_pickle(to_pickle, filename, zip=False):
    if zip:
        with gzip.open(data_folder / (filename + ".pkl"), 'ab') as file:
            pickle.dump(to_pickle, file)
    else:
        with open(data_folder / filename, 'ab') as file:
            pickle.dump(to_pickle, file)


def load_csv(readfile, date_column_name, chunk_size=10000):
    for chunk in pandas.read_table(data_folder / readfile, encoding="ISO-8859-1", sep='\t', chunksize=chunk_size,
                                   lineterminator='\n',
                                   parse_dates=[date_column_name], infer_datetime_format=True, low_memory=False):
        yield chunk


def load_entire_df_pickle(readfile):
    """
    Loads a pickle file of dataframes and puts them together into one big dataframe

    :param readfile: the file path that should be read from
    :return: the concatenated dataframe
    """

    frames = []
    try:
        for frame in load_pickle_iterate(readfile):
            frames.append(frame)
    except EOFError:
        pass
    df = pandas.concat(frames, sort=False)
    return df


def load_pickle_iterate(readfile):
    """
    Generator for objects in a pickle file

    :param readfile: the file path that should be read from
    :return: a generator for the objects in the file
    :exception EOFError when there are no more objects
    """
    with open(data_folder / readfile, 'rb') as infile:
        while True:
            yield pickle.load(infile)


def read_pickle(readfile):
    with open(data_folder / readfile, 'rb') as infile:
        return pickle.load(infile)


def log_predictor_result(row, writefile):
    # open the file in the write mode
    append_header = False
    if not os.path.isfile(log_folder / writefile):
        append_header = True

    now = datetime.now()
    time_string = now.strftime("%H:%M:%S")

    row = list(map(format_number, row))

    with open(log_folder / writefile, 'a', newline="") as f:
        writer = csv.writer(f)
        if append_header:
            writer.writerow(csv_header)
        writer.writerow([time_string] + row)


def log_predictor_result_shortened(row, writefile):
    # open the file in the write mode
    append_header = False
    if not os.path.isfile(log_folder / writefile):
        append_header = True

    row = list(map(format_number, row))

    with open(log_folder / writefile, 'a', newline="") as f:
        writer = csv.writer(f)
        if append_header:
            writer.writerow(csv_header_shortened)
        writer.writerow(row)


def format_number(item):
    try:
        return "{:20.4f}".format(float(item))
    except TypeError:
        return item
    except ValueError:
        return item


if __name__ == "__main__":
    save_as_pickle("pls", "test")
